from flask import Flask
from flask import render_template
from flask import request

import time
import requests
from lxml import objectify
from lxml.html import fromstring
from markupsafe import escape

app = Flask(__name__)

def get_games(url):
  #print(url)
  while True:
    r = requests.get(url)

    # 202 means the collection isn't ready yet
    # TODO: add some more error handling
    while r.status_code != 200:
      time.sleep(3)

    # Parse the xml into a python object.
    # We need to replace the 'encoding' declaration specified in the xml because lxml hates it.
    return objectify.fromstring(r.text.replace('encoding="utf-8" ', ''))

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/results/', methods=['POST'])
def results(name=None):
  if request.method == 'POST':
    name = request.form['username']
    mygames = get_games("https://boardgamegeek.com/xmlapi/collection/%s?own=1" % name)
    #import pdb; pdb.set_trace()
    #if not len(mygames.item):
    #  # We didn't find any results for that user.
    #  return render_template('hello.html')

    printable_games = get_games("http://boardgamegeek.com/xmlapi/geeklist/186909?comments=1")

    game_ids = []

    for game in mygames.item:
      #print(game.attrib['objectid'], game.name)
      game_ids.append(game.attrib['objectid'])

    matching_games = [ game_list for game_list in printable_games.item if game_list.attrib['objectid'] in game_ids ]
    matching_games.sort(key=lambda x: x.attrib['objectname'], reverse=False)

  return render_template('results.html', 
                          name=name,
                          total_games=str(mygames.countchildren()),
                          matching_games=str(len(matching_games)))


@app.route('/user/<username>')
def show_user_profile(username):
  # show the user profile for that user
  return 'User %s' % escape(username)

